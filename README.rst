##########
mangascans
##########

Convenience tools for mangascans.


Zip
---

Pack mangascans folder into zip file.


Installation
============

.. code-block:: bat

  (.venv) > pip install git+https://gitlab.com/julot/mangascans.git#egg=mangascans


Usage
=====

.. code-block:: bat

  (.venv) > mangascans --help


Changes
=======

0.1.1
-----

* Add --shallow flag to zip command.


0.1.0
-----

* First public release.
