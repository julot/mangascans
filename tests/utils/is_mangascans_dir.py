import logging

from mangascans.utils import is_mangascans_dir


def test():
    logger = logging.getLogger('mangascans.utils')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

    invalid_dirs = ['.', '..']
    valid_files = ['1.jpg', '2.jpeg', '3.png', '4.gif', '5.tif', '6.tiff',
                   '7.bmp']
    invalid_files = ['1.txt', '2.psd']

    logger.info('Test with invalid dirs and invalid files')
    assert not is_mangascans_dir(invalid_dirs, invalid_files)

    logger.info('Test with invalid dirs and valid files')
    assert not is_mangascans_dir(invalid_dirs, valid_files)

    logger.info('Test with valid dirs and invalid files')
    assert not is_mangascans_dir([], invalid_files)

    logger.info('Test with valid dirs and valid files')
    assert is_mangascans_dir([], valid_files)
