"""Utils."""

import os
import logging


IMAGE_EXTS = ('.jpg', '.jpeg', '.png', '.gif', '.tif', '.tiff', '.bmp')


def is_mangascans_dir(dirs, files):
    """
    Check whether root directory is mangascans directory or nat.

    Mangascans directory is specified by the lack of dirs and contains only
    image files.

    """
    logger = logging.getLogger(__name__)

    if dirs:
        logger.debug('Contains sub directory: "{0}"'.format(dirs))
        return False

    if not files:
        logger.debug('No files.')
        return False

    for file in files:
        _, ext = os.path.splitext(file)
        if ext not in IMAGE_EXTS:
            logger.debug('Files has non image file: {0}'.format(ext))
            return False

    return True
