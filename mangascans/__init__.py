"""MangaScans."""

import os
import click
import clicking
import zipfile
import stat

from click_help_colors import HelpColorsGroup
from mangascans.utils import is_mangascans_dir


__version__ = '0.1.1'

HELP = """
    \b
    MangaScans
    Version {0}

    Convenience tools to works with mangascans.

    """

EPILOG = '(c) 2017-2018 Andy Yulius <andy.julot@gmail.com>'


class Error(click.ClickException):
    """Error exception."""

    def show(self, file=None):
        """Print error message in red color."""
        clicking.fail('Error: %s' % self.format_message())


@click.group(
    cls=HelpColorsGroup,
    help_headers_color='yellow',
    help_options_color='green',
    help=HELP.format(__version__),
    epilog=EPILOG,
)
def cli():
    """Main entry."""
    pass


@cli.command(
    name='zip',
    short_help='Pack mangascans directory into zip file recursively.',
    epilog=EPILOG,
)
@click.argument(
    'path',
    type=click.Path(
        exists=True,
        file_okay=False,
        writable=True,
        resolve_path=True,
    ),
)
@click.option(
    '--shallow',
    '-s',
    is_flag=True,
    help=(
        'Pack all directory in PATH into zip file. '
        'Used to pack volume with chapter as sub directory.'
    ),
)
def pack(path, shallow):
    """
    Traverse directory recursively to find directory with only image files,
    then pack it into zip file.

    The zip file will be saved at the location where the directory resides.

    Warning: The directory will be removed.

    """
    if shallow:
        pack_shallow(path=path)
        return

    pack_recursive(path=path)


def pack_recursive(path):
    for root, dirs, files in os.walk(path, topdown=False):
        clicking.working('Checking "{0}"... '.format(root), nl=False)
        if not is_mangascans_dir(dirs, files):
            clicking.fail('Not mangascans dir.')
            continue

        clicking.success('Mangascans dir.')

        compress(files=[os.path.join(root, f) for f in files], name=root)
        delete_dir(root)


def pack_shallow(path):
    join = os.path.join
    folders = [f for f in os.listdir(path) if os.path.isdir(join(path, f))]
    for folder in folders:
        files = pluck_files(path=join(path, folder))
        if not files:
            continue

        compress(files=files, name=os.path.join(path, folder))
        delete_dir(path=os.path.join(path, folder))


def pluck_files(path):
    """Get all files from path including files in the sub directory."""

    content = []

    for root, dirs, files in os.walk(path, topdown=False):
        for file in files:
            if file.lower() == 'thumb.db':
                continue

            content.append(os.path.join(root, file))

    return content


def compress(files, name):
    """Compress FILES to NAME.zip file."""

    head, tail = os.path.split(name)
    clicking.progress('Zipping folder "{0}"...'.format(tail))
    filename = os.path.join(head, '{0}.zip'.format(tail))
    prefix = len(name) + 1
    try:
        with click.progressbar(
            iterable=files,
            show_eta=True,
        ) as bar:
            with zipfile.ZipFile(filename, mode='w') as f:
                for file in bar:
                    f.write(
                        filename=file,
                        arcname=file[prefix:],
                    )
    except Exception as e:
        clicking.fail('\n{0}'.format(str(e)))
        exc = Error('Zip operation failed. Please review the message.')
        raise exc


def delete_dir(path):
    clicking.working('Deleting directory... ', nl=False)
    try:
        remove_tree(path)
    except Exception as e:
        clicking.fail('Error')
        clicking.fail(str(e))
        exc = Error('Delete failed. Please review the message.')
        raise exc
    else:
        clicking.success('OK')


def remove_tree(path):
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            filename = os.path.join(root, name)
            os.chmod(filename, stat.S_IWUSR)
            os.remove(filename)

        for name in dirs:
            os.rmdir(os.path.join(root, name))

    os.rmdir(path)
